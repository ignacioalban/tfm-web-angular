import { Injectable } from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors, ValidatorFn } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CustomeValidatorsService {

  constructor() { }
}

export function ValidarRango(value: number): ValidatorFn{
  return (control: AbstractControl): ValidationErrors | null =>{
    let fin = control.value;
    let inicio = value;

    if(inicio < fin){
      return {'ValidarRango': true};
    }else{
      return {'ValidarRango': false};
    }

    return null;
  }
}