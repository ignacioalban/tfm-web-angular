import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, Validators, ValidationErrors, ValidatorFn, AbstractControl } from '@angular/forms';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IObjetos, Respuesta, Resultado, ElementosTabla, TmpProcesamiento, ElementosErrorOrden } from 'src/common/IObjetos';
import { HttpHeaders } from '@angular/common/http';
import { DataSource } from '@angular/cdk/collections';
import { NULL_EXPR } from '@angular/compiler/src/output/output_ast';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogContainer } from '@angular/material/dialog';
import { KatexOptions } from 'ng-katex';

import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label, BaseChartDirective } from 'ng2-charts';
//import zoomPlugin from 'chartjs-plugin-zoom';

import 'chartjs-plugin-zoom';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ProgressDialogComponent } from './progress-dialog/progress-dialog.component';
import { ErrorDialogComponent } from './error-dialog/error-dialog.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  template: '<ng-katex [equation]="equation"></ng-katex>',
  styleUrls: ['./app.component.css']
})


//export class AppComponent implements OnInit {
export class AppComponent implements AfterViewInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  constructor(private httpClient: HttpClient, private global:IObjetos, public dialog: MatDialog ){}

  title = 'Método de Taylor con Heyoka';
  BASEURL = 'http://localhost:8085';

  equation : string[] = ['error=\\max(abs({y_{k}^N-Y_{1+2k}^{2N}}))',
                         'orden=\\log_{2}\\left({\\frac{error_{N}}{error_{N + 1}}}\\right)',
                         'error=Solex_{k} - Y_{k}'];
  options: KatexOptions = {
    displayMode: true,
  };
  frmControles = new FormGroup({
    ecuacion : new FormControl('',[Validators.required]),
    usaSolex : new FormControl(false),
    solex: new FormControl('',[Validators.required]),
    derivadas : new FormControl(''),
    inicial :  new FormControl('',[Validators.required]),
    final :  new FormControl('',[Validators.required]),
    divisiones :  new FormControl('',
            [Validators.required, 
             Validators.min(5)]),
    valoresIniciales :  new FormControl('',[Validators.required]),
    euler : new FormControl(false),
    heun : new FormControl(false),
    ralston : new FormControl(false),
    rungekutta : new FormControl(false),
    rkbutcher : new FormControl(false),
    adamsbashford : new FormControl(false),
    adamsmoulton : new FormControl(false),
    abmoulton : new FormControl(false),
    heyoka : new FormControl(false),
    todos : new FormControl(false)
  },
  {
    validators: [
      this.ValidarMenorA('final','inicial'),
      this.ValidarMayorA('inicial','final'),
    ]
  });

  muestraDerivadas : boolean = false;
  muestraErrorAbsoluto: boolean = false;
  metodoSeleccionado : boolean = false;
  tieneSolex : boolean = false;

  dtSource: ElementosTabla[] = [];
  dtTableModel: MatTableDataSource<ElementosTabla> = new MatTableDataSource();
  dtError : ElementosErrorOrden[] = [];
  dtOrden : ElementosErrorOrden[] = [];
  dtAbError : ElementosErrorOrden[] = [];

  dtTiempos: Array<TmpProcesamiento> = [{
    Euler : 0,
    Heun : 0,
    Ralston : 0,
    RungeKutta : 0,
    RKButcher : 0,
    AdamBashford : 0,
    AdamMoulton : 0,
    AdamBashfordMoulton : 0,
    Heyoka : 0
  }];

  displayedColumns = ['X','Solex','Euler','Heun','Ralston','RungeKutta','RKButcher','AdamBashford','AdamMoulton','AdamBashfordMoulton','Heyoka'];
  displayedColumnsErrorOrden = ['N','Euler','Heun','Ralston','RungeKutta','RKButcher','AdamBashford','AdamMoulton','AdamBashfordMoulton','Heyoka'];
  displayedColumnsTiempo = ['Euler','Heun','Ralston','RungeKutta', 'RKButcher','AdamBashford','AdamMoulton','AdamBashfordMoulton','Heyoka'];

  dataX : number[] = [];
  dataSeries: ChartDataSets[] = [];
  dataSeriesTiempo: ChartDataSets[]= [];
  lineChartLabels: Label[] = [];
  tiempoChartLabels: Label[] = [];
  barChartOptions = {
    responsive: true,
    scales:{
      yAxes:[{
        scaleLabel:{
          display: true,
          labelString: 'Tiempo de procesamiento'
        }
      }],
      xAxes:[{
        scaleLabel:{
          display: true,
          labelString: 'Métodos Numéricos'
        }
      }]
    },
    animation: {
      duration : 0
    }
  };
  lineChartOptions = {
    responsive: true,
    scales:{
      yAxes:[{
        scaleLabel:{
          display: true,
          labelString: 'f(x)'
        }
      }],
      xAxes:[{
        scaleLabel:{
          display: true,
          labelString: 'x'
        }
      }]
    },
    animation: {
      duration : 0
    },
    plugins:{
      zoom:{
        pan: {
          enabled: true,
          mode: 'xy',
          onPan: function () { console.log('I was panned!!!'); }
        },  
        zoom:{
          wheel:{
            enable: true
          },
          pinch:{
            enable: true
          },
          enable: true,
          drag: true,
          mode: 'xy',
          onZoom: function(){ console.log('listo');}
        }
      }
    }
  };
  lineChartColors: Color[] = [];
  barChartColors: Color[] = [];
  lineChartLegend = true;
  barChartLegend = true;
  lineChartPlugins = [];
  lineChartType : ChartType= 'line';
  barChartType : ChartType='bar';

  dialogRef = this.dialog;
  dialogErr = this.dialog;

  
  ValidarMenorA(firstControl:any, secondControl:any): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const vfinal = control.get(firstControl)?.value;
      const vinicio = control.get(secondControl)?.value;
      
      if(vinicio >= vfinal){
        control.get(firstControl)?.setErrors({notValid: true})
        return {notValid: true};
      }
      return null
    }
  }

  ValidarMayorA(firstControl:any, secondControl:any): ValidatorFn {

    return (control: AbstractControl): ValidationErrors | null => {

      const vfinal = control.get(firstControl)?.value;
      const vinicio = control.get(secondControl)?.value;
      
      if(vinicio <= vfinal){
        control.get(firstControl)?.setErrors({notValid: true})
        return {notValid: true};
      }

      return null
    }
  }

  activaSolex(){
    const vSolex = this.frmControles.get('usaSolex')?.value;
    this.tieneSolex = vSolex;
    if (vSolex == true){
      this.frmControles.controls['solex'].enable();
      this.frmControles.controls['solex'].addValidators(Validators.required);
      this.frmControles.controls['solex'].setErrors({ required:true});
      
    }else{
      this.frmControles.controls['solex'].disable();
      this.frmControles.controls['solex'].clearValidators();
    }
  }

  seleccionaTodos(){
    const vTodos = this.frmControles.get('todos')?.value;
    let selecciona = false;
    let event = new MatCheckboxChange();

    event.checked = false;
    if(vTodos == true) {
      selecciona = true;
      event.checked = true;
    }

    this.frmControles.controls['euler'].setValue(selecciona);
    this.frmControles.controls['heun'].setValue(selecciona);
    this.frmControles.controls['ralston'].setValue(selecciona);
    this.frmControles.controls['rungekutta'].setValue(selecciona);
    this.frmControles.controls['rkbutcher'].setValue(selecciona);
    this.frmControles.controls['adamsbashford'].setValue(selecciona);
    this.frmControles.controls['adamsmoulton'].setValue(selecciona);
    this.frmControles.controls['abmoulton'].setValue(selecciona);
    this.frmControles.controls['heyoka'].setValue(selecciona);


    this.usaDerivadas(event);
    this.validaSeleccion();
  }

  validaSeleccion(){
    const vEuler = this.frmControles.get('euler')?.value;
    const vHeun = this.frmControles.get('heun')?.value;
    const vRalston = this.frmControles.get('ralston')?.value;
    const vRungeKutta = this.frmControles.get('rungekutta')?.value;
    const vRKButcher = this.frmControles.get('rkbutcher')?.value;
    const vAdamBashford = this.frmControles.get('adamsbashford')?.value;
    const vAdamMoulton = this.frmControles.get('adamsmoulton')?.value;
    const vAdamBashfordMoulton = this.frmControles.get('abmoulton')?.value;
    const vHeyoka = this.frmControles.get('heyoka')?.value;

    if (vEuler == false && vHeun == false &&
        vRalston == false && vRungeKutta == false && vRKButcher == false &&
        vAdamBashford == false && vAdamBashfordMoulton == false &&
        vAdamMoulton == false && vHeyoka == false){
      this.frmControles.setErrors({'MethodNoSelect':true});
    }else{
      this.frmControles.setErrors({'MethodNoSelect':null});
      this.frmControles.updateValueAndValidity();
      this.metodoSeleccionado = true;
    }
  }

  limpiar(): void{
    this.frmControles.controls['ecuacion'].setValue('');
    this.frmControles.controls['valoresIniciales'].setValue('');
    this.frmControles.controls['derivadas'].setValue('');
    this.frmControles.controls['inicial'].setValue(0);
    this.frmControles.controls['final'].setValue(1);
    this.frmControles.controls['divisiones'].setValue(5);

    this.frmControles.controls['euler'].setValue(false);
    this.frmControles.controls['heun'].setValue(false);
    this.frmControles.controls['ralston'].setValue(false);
    this.frmControles.controls['rungekutta'].setValue(false);
    this.frmControles.controls['rkbutcher'].setValue(false);
    this.frmControles.controls['adamsbashford'].setValue(false);
    this.frmControles.controls['adamsmoulton'].setValue(false);
    this.frmControles.controls['abmoulton'].setValue(false);
    this.frmControles.controls['heyoka'].setValue(false);

    this.metodoSeleccionado = false;

    
    this.frmControles.controls['todos'].setValue(false);
    this.frmControles.controls['usaSolex'].setValue(false);
    this.frmControles.controls['solex'].setValue('');
    this.activaSolex();
    this.seleccionaTodos();
  }

  ngOnInit(): void{
    this.limpiar();
    this.activaSolex();
    
    this.paginator._intl.itemsPerPageLabel = "Items por página";
  } 

  ngAfterViewInit(){
    this.dtTableModel.paginator = this.paginator;
  }

  public chartColors(){
    return this.lineChartColors;
  }

  usaDerivadas(event: MatCheckboxChange){
    this.muestraDerivadas = event.checked;

    if (this.muestraDerivadas){
      this.frmControles.controls['derivadas'].addValidators(Validators.required);
      this.frmControles.controls['derivadas'].setErrors({required:true})
    }else{
      this.frmControles.controls['derivadas'].clearValidators();
    }
  }


  openDialog(){
    this.dialogRef.open(ProgressDialogComponent,{
      width: '400px',
      height: '300px',
      disableClose: true
    })
  }

  openErrorDialog(mensaje: string){
    this.dialogErr.open(ErrorDialogComponent,{
      width: '400px',
      height: '300px',
      data:{
        Mensaje: mensaje
      }
    })
  }

  cancelar(event: Event){
    this.limpiar();
  }

  ejecutar(event: Event){
    this.openDialog();
    let valores = {
      funcion: this.frmControles.controls['ecuacion'].value,
      derivada: this.frmControles.controls['derivadas'].value,
      ii : this.frmControles.controls['inicial'].value,
      if : this.frmControles.controls['final'].value,
      vi : this.frmControles.controls['valoresIniciales'].value,
      eu: this.frmControles.controls['euler'].value,
      he: this.frmControles.controls['heun'].value,
      rl: this.frmControles.controls['ralston'].value,
      rk: this.frmControles.controls['rungekutta'].value,
      rk5: this.frmControles.controls['rkbutcher'].value,
      ab: this.frmControles.controls['adamsbashford'].value,
      am: this.frmControles.controls['adamsmoulton'].value,
      abm: this.frmControles.controls['abmoulton'].value,
      hyk: this.frmControles.controls['heyoka'].value,
      N: this.frmControles.controls['divisiones'].value,
      solex: this.frmControles.controls['solex'].value
    };

    this.httpClient.post<Respuesta>(this.BASEURL+'/comparacionMetodos', valores)
    .subscribe( data => {
      this.dtSource = [];
      this.dtTiempos = [];
      this.dtError = [];
      this.dtOrden = [];
      this.dtAbError = [];
      this.lineChartLabels = [];
      this.tiempoChartLabels = [];
      this.lineChartColors = [];
      this.dataSeries = [];
      this.dataSeriesTiempo = [];
      this.dataSeries.shift();
      this.displayedColumns = [];
      this.displayedColumnsTiempo = [];
      this.displayedColumnsErrorOrden = ['N'];

      let cantidad = data.cantidad;
      this.dataX = data.x;
      let dataSolex = data.Solex;
      let dataEuler = data.Euler;
      let dataHeun = data.Heun;
      let dataRalston = data.Ralston;
      let dataRungeKutta = data.RungeKutta;
      let dataRungeKutta5 = data.RKButcher;
      let dataAdamBashford = data.AdamBashford;
      let dataAdamMoulton = data.AdamMoulton;
      let dataAdamBashfordMoulton = data.AdamBashfordMoulton;
      let dataHeyoka = data.Heyoka;
      
      for(let i=0; i<cantidad; i++){
        this.dtSource.push({X:0, 
          Solex:0,
          Euler:0,
          Heun:0,
          Ralston: 0,
          RungeKutta:0,
          RKButcher: 0,
          AdamBashford:0,
          AdamMoulton:0,
          AdamBashfordMoulton: 0,
          Heyoka:0
        });
      }

      if(data.x != null){
        for(let i=0; i<cantidad; i++){
          this.dtSource[i].X=this.dataX[i];
          this.lineChartLabels.push(this.dataX[i].toFixed(3).toString());
        }
      }

      if(data.Solex.resultado != null){
        this.muestraErrorAbsoluto = true;
        for(let i=0; i<cantidad; i++){
          this.dtSource[i].Solex=dataSolex.resultado[i];
        }
      }

      let dEuler: number[] = [];
      if(data.Euler.resultado != null){
        for(let i=0; i<cantidad; i++){
          this.dtSource[i].Euler=dataEuler.resultado[i][0];
          dEuler.push(dataEuler.resultado[i][0]);
        }
      }

      let dHeun: number[] = [];
      if(data.Heun.resultado != null){
        for(let i=0; i<cantidad; i++){
          this.dtSource[i].Heun=dataHeun.resultado[i][0];
          dHeun.push(dataHeun.resultado[i][0]);
        }
      }

      let dRalston: number[] = [];
      if(data.Ralston.resultado != null){
        for(let i=0; i<cantidad; i++){
          this.dtSource[i].Ralston=dataRalston.resultado[i][0];
          dRalston.push(dataRalston.resultado[i][0]);
        }
      }

      let dRungeKutta: number[] = [];
      if(data.RungeKutta.resultado != null){
        for(let i=0; i<cantidad; i++){
          this.dtSource[i].RungeKutta=dataRungeKutta.resultado[i][0];
          dRungeKutta.push(dataRungeKutta.resultado[i][0]);
        }
      }

      let dRungeKutta5: number[] = [];
      if(data.RKButcher.resultado != null){
        for(let i=0; i<cantidad; i++){
          this.dtSource[i].RKButcher=dataRungeKutta5.resultado[i][0];
          dRungeKutta5.push(dataRungeKutta5.resultado[i][0]);
        }
      }

      let dAdamBashford: number[] = [];
      if(data.AdamBashford.resultado != null){
        for(let i=0; i<cantidad; i++){
          this.dtSource[i].AdamBashford=dataAdamBashford.resultado[i][0];
          dAdamBashford.push(dataAdamBashford.resultado[i][0]);
        }
      }

      let dAdamMoulton: number[] = [];
      if(data.AdamMoulton.resultado != null){
        for(let i=0; i<cantidad; i++){
          this.dtSource[i].AdamMoulton=dataAdamMoulton.resultado[i][0];
          dAdamMoulton.push(dataAdamMoulton.resultado[i][0]);
        }
      }

      let dAdamBashfordMoulton: number[] = [];
      if(data.AdamBashfordMoulton.resultado != null){
        for(let i=0; i<cantidad; i++){
          this.dtSource[i].AdamBashfordMoulton=dataAdamBashfordMoulton.resultado[i][0];
          dAdamBashfordMoulton.push(dataAdamBashfordMoulton.resultado[i][0]);
        }
      }

      let dHeyoka: number[] = [];
      if(data.Heyoka.resultado != null){
        for(let i=0; i<cantidad; i++){
          this.dtSource[i].Heyoka=dataHeyoka.resultado[i][0];
          dHeyoka.push(dataHeyoka.resultado[i][0]);
        }
      }

      let NValor = 20;
      for(let i=0; i< 6; i++){
        this.dtError.push({
          N : NValor,
          Euler: data.Euler.error == null ? '' : data.Euler.error[i],
          Heun: data.Heun.error == null ? '' : data.Heun.error[i],
          Ralston: data.Ralston.error == null ? '' : data.Ralston.error[i],
          RungeKutta: data.RungeKutta.error == null ? '' : data.RungeKutta.error[i],
          RKButcher: data.RKButcher.error == null ? '' : data.RKButcher.error[i],
          AdamBashford: data.AdamBashford.error == null? '' :data.AdamBashford.error[i],
          AdamMoulton: data.AdamMoulton.error == null? '' :data.AdamMoulton.error[i],
          AdamBashfordMoulton: data.AdamBashfordMoulton.error == null? '' :data.AdamBashfordMoulton.error[i],
          Heyoka: data.Heyoka.error == null? '' :data.Heyoka.error[i]
        });

        this.dtOrden.push({
          N : NValor,
          Euler: data.Euler.orden == null ? '': data.Euler.orden[i],
          Heun: data.Heun.orden == null ? '' : data.Heun.orden[i],
          Ralston: data.Ralston.orden == null ? '' : data.Ralston.orden[i],
          RungeKutta: data.RungeKutta.orden == null ? '' : data.RungeKutta.orden[i],
          RKButcher: data.RKButcher.orden == null ? '' : data.RKButcher.orden[i],
          AdamBashford: data.AdamBashford.orden == null ? '' : data.AdamBashford.orden[i],
          AdamMoulton: data.AdamMoulton.orden == null ? '' : data.AdamMoulton.orden[i],
          AdamBashfordMoulton: data.AdamBashfordMoulton.orden == null ? '' : data.AdamBashfordMoulton.orden[i],
          Heyoka: data.Heyoka.orden == null? '' :data.Heyoka.orden[i]
        });

        if(this.tieneSolex){
          this.dtAbError.push({
            N : NValor,
            Euler: data.Euler.errorAb == null ? '': data.Euler.errorAb[i],
            Heun: data.Heun.errorAb == null ? '' : data.Heun.errorAb[i],
            Ralston: data.Ralston.errorAb == null ? '' : data.Ralston.errorAb[i],
            RungeKutta: data.RungeKutta.errorAb == null ? '' : data.RungeKutta.errorAb[i],
            RKButcher: data.RKButcher.errorAb == null ? '' : data.RKButcher.errorAb[i],
            AdamBashford: data.AdamBashford.errorAb == null ? '' : data.AdamBashford.errorAb[i],
            AdamMoulton: data.AdamMoulton.errorAb == null ? '' : data.AdamMoulton.errorAb[i],
            AdamBashfordMoulton: data.AdamBashfordMoulton.errorAb == null ? '' : data.AdamBashfordMoulton.errorAb[i],
            Heyoka: data.Heyoka.errorAb == null? '' :data.Heyoka.errorAb[i]
          });
        }

        NValor = NValor * 2;
      }

      this.dtTiempos.push
      (
        {
          Euler: data.Euler.tiempo==null? 0: data.Euler.tiempo,
          Heun: data.Heun.tiempo==null ? 0: data.Heun.tiempo,
          Ralston: data.Ralston.tiempo==null?0:data.Ralston.tiempo,
          RungeKutta: data.RungeKutta.tiempo==null?0:data.RungeKutta.tiempo,
          RKButcher: data.RKButcher.tiempo==null?0:data.RKButcher.tiempo,
          AdamBashford: data.AdamBashford.tiempo==null?0:data.AdamBashford.tiempo,
          AdamMoulton: data.AdamMoulton.tiempo==null?0:data.AdamMoulton.tiempo,
          AdamBashfordMoulton: data.AdamBashfordMoulton.tiempo==null?0:data.AdamBashfordMoulton.tiempo,
          Heyoka: data.Heyoka.tiempo == null? 0 :data.Heyoka.tiempo
        }
      );
      
      if(this.frmControles.controls['euler'].value == true) this.displayedColumnsTiempo.push('Euler');
      if(this.frmControles.controls['heun'].value == true) this.displayedColumnsTiempo.push('Heun');
      if(this.frmControles.controls['ralston'].value == true) this.displayedColumnsTiempo.push('Ralston');
      if(this.frmControles.controls['rungekutta'].value == true) this.displayedColumnsTiempo.push('RungeKutta');
      if(this.frmControles.controls['rkbutcher'].value == true) this.displayedColumnsTiempo.push('RKButcher');
      if(this.frmControles.controls['adamsbashford'].value == true) this.displayedColumnsTiempo.push('AdamBashford');
      if(this.frmControles.controls['adamsmoulton'].value == true) this.displayedColumnsTiempo.push('AdamMoulton');
      if(this.frmControles.controls['abmoulton'].value == true) this.displayedColumnsTiempo.push('AdamBashfordMoulton');
      if(this.frmControles.controls['heyoka'].value == true) this.displayedColumnsTiempo.push('Heyoka');

      this.displayedColumns.push('X');
      if(data.Solex.resultado != null) this.displayedColumns.push('Solex');
      if(this.frmControles.controls['euler'].value == true) this.displayedColumns.push('Euler');
      if(this.frmControles.controls['heun'].value == true) this.displayedColumns.push('Heun');
      if(this.frmControles.controls['ralston'].value == true) this.displayedColumns.push('Ralston');
      if(this.frmControles.controls['rungekutta'].value == true) this.displayedColumns.push('RungeKutta');
      if(this.frmControles.controls['rkbutcher'].value == true) this.displayedColumns.push('RKButcher');
      if(this.frmControles.controls['adamsbashford'].value == true) this.displayedColumns.push('AdamBashford');
      if(this.frmControles.controls['adamsmoulton'].value == true) this.displayedColumns.push('AdamMoulton');
      if(this.frmControles.controls['abmoulton'].value == true) this.displayedColumns.push('AdamBashfordMoulton');
      if(this.frmControles.controls['heyoka'].value == true) this.displayedColumns.push('Heyoka');
      
      if(this.frmControles.controls['euler'].value == true) this.displayedColumnsErrorOrden.push('Euler');
      if(this.frmControles.controls['heun'].value == true) this.displayedColumnsErrorOrden.push('Heun');
      if(this.frmControles.controls['ralston'].value == true) this.displayedColumnsErrorOrden.push('Ralston');
      if(this.frmControles.controls['rungekutta'].value == true) this.displayedColumnsErrorOrden.push('RungeKutta');
      if(this.frmControles.controls['rkbutcher'].value == true) this.displayedColumnsErrorOrden.push('RKButcher');
      if(this.frmControles.controls['adamsbashford'].value == true) this.displayedColumnsErrorOrden.push('AdamBashford');
      if(this.frmControles.controls['adamsmoulton'].value == true) this.displayedColumnsErrorOrden.push('AdamMoulton');
      if(this.frmControles.controls['abmoulton'].value == true) this.displayedColumnsErrorOrden.push('AdamBashfordMoulton');
      if(this.frmControles.controls['heyoka'].value == true) this.displayedColumnsErrorOrden.push('Heyoka');


      this.dtTableModel.data = this.dtSource;
      this.dtTableModel.paginator = this.paginator;

      if(dEuler.length > 0) {
        this.dataSeries.push({data: dEuler, label: 'Euler'});
        this.dataSeriesTiempo.push({data: [data.Euler.tiempo], label: 'Euler'});
        this.lineChartColors.push({ borderColor:'black', backgroundColor:'rgba(0,0,0,0.3)'});
      }

      if(dHeun.length > 0) {
        this.dataSeries.push({data: dHeun, label: 'Heun'});
        this.dataSeriesTiempo.push({data: [data.Heun.tiempo], label: 'Heun'});
        this.lineChartColors.push({ borderColor:'red', backgroundColor:'rgba(255,0,0,0.3)'});
      }
      if(dRalston.length > 0) {
        this.dataSeries.push({data: dRalston, label: 'Ralston'});
        this.dataSeriesTiempo.push({data: [data.Ralston.tiempo], label: 'Ralston'});
        this.lineChartColors.push({ borderColor:'blue', backgroundColor:'rgba(0,0,255,0.3)'});
        
      };
      if(dRungeKutta.length > 0) {
        this.dataSeries.push({data: dRungeKutta, label: 'Runge-Kutta'});
        this.dataSeriesTiempo.push({data: [data.RungeKutta.tiempo], label: 'Runge Kutta'});
        this.lineChartColors.push({ borderColor:'green', backgroundColor:'rgba(0,128,0,0.3)'});
      }
      if(dRungeKutta5.length > 0) {
        this.dataSeries.push({data: dRungeKutta5, label: 'RK Butcher'});
        this.dataSeriesTiempo.push({data: [data.RKButcher.tiempo], label: 'RK Butcher'});
        this.lineChartColors.push({ borderColor:'orange', backgroundColor:'rgba(223,141,109,1)'});
      }
      if(dAdamBashford.length > 0) {
        this.dataSeries.push({data: dAdamBashford, label: 'Adam-Bashford'});
        this.dataSeriesTiempo.push({data: [data.AdamBashford.tiempo], label: 'Adam-Bashford'});
        this.lineChartColors.push({ borderColor:'cyan', backgroundColor:'rgba(0,255,255,0.3)'});
      }
      if(dAdamMoulton.length > 0) {
        this.dataSeries.push({data: dAdamMoulton, label: 'Adam-Moulton'});
        this.dataSeriesTiempo.push({data: [data.AdamMoulton.tiempo], label: 'Adam-Moulton'});
        this.lineChartColors.push({ borderColor:'brown', backgroundColor:'rgba(193,130,83,0.7)'});
      }
      if(dAdamBashfordMoulton.length > 0) {
        this.dataSeries.push({data: dAdamBashfordMoulton, label: 'AB-Moulton'});
        this.dataSeriesTiempo.push({data: [data.AdamBashfordMoulton.tiempo], label: 'AB-Moulton'});
        this.lineChartColors.push({ borderColor:'purple', backgroundColor:'rgba(145,54,130,0.56)'});
      }
      if(dHeyoka.length > 0) {
        this.dataSeries.push({data: dHeyoka, label: 'Heyoka'});
        this.dataSeriesTiempo.push({data: [data.Heyoka.tiempo], label: 'Heyoka'});
        this.lineChartColors.push({ borderColor:'blue', backgroundColor:'rgba(11,47,91,0.56)'});
      }
      
      this.chart.chart.update();
      this.chart.updateColors();

      this.dialogRef.closeAll();
    },
    error =>{
      let ErrorFromServer = error.error.error;
      this.dialogRef.closeAll();

      this.openErrorDialog(ErrorFromServer);
    });
  }

}