import { Injectable } from "@angular/core";

@Injectable({
    providedIn : 'root'
})
export class IObjetos{
    
}

export interface ErrorMessage{
    Mensaje : string
}

export interface ResultadoBase{
    x: number,
    resultado: any
}

export interface Resultado{
    x: number,
    resultado: any,
    tiempo: number,
    error: any,
    orden: any,
    errorAb: any
}

export interface Respuesta{
    x: any,
    cantidad : number,
    Solex: ResultadoBase,
    Euler: Resultado,
    Heun: Resultado,
    Ralston: Resultado,
    RungeKutta: Resultado,
    RKButcher: Resultado,
    AdamBashford: Resultado,
    AdamMoulton: Resultado,
    AdamBashfordMoulton: Resultado,
    Heyoka: Resultado
}

export interface ElementosErrorOrden{
    N : number,
    Euler: string,
    Heun: string,
    Ralston: string,
    RungeKutta: string,
    RKButcher: string,
    AdamBashford: string,
    AdamMoulton: string,
    AdamBashfordMoulton: string,
    Heyoka: string
}

export interface ElementosTabla{
    X : number,
    Solex: number,
    Euler: number,
    Heun: number,
    Ralston: number,
    RungeKutta: number,
    RKButcher: number,
    AdamBashford: number,
    AdamMoulton: number,
    AdamBashfordMoulton: number,
    Heyoka: number
}

export interface TmpProcesamiento{
    Euler: number,
    Heun: number,
    Ralston: number,
    RungeKutta: number,
    RKButcher : number,
    AdamBashford: number,
    AdamMoulton: number,
    AdamBashfordMoulton: number,
    Heyoka: number
}